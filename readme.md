# The ultime task manager

L'idée derrière ce projet est d'avoir un créateur de tâche simple, et efficace qui permet de mieux ranger sa vie perso et pro.

L'idée est de regroupé les deux en une seule app, permettant ainsi d'aller à un seul endroit.

On souhaite pour se projet en faire une web app simple, donc l'idée est de se rapproché le plus possible d'un éditeur de note.

Le but est de pouvoir, à partir d'un markdown le parser, en créer des tâches et le lancé dans un projet.

Depuis là une vue, de type "graph" permettrait de visualiser tous ces projets en cours et les tâches dépendante les plus longue.

L'idée est de pouvoir offrir un large choix de vue possible pour l'utilisateur pour qu'il puisse s'organiser au mieux.

J'aimerais pouvoir arrêter d'avoir des markdown avec des checkbox ou des readme dans mon code, mais un endroit simple où je peux inserer facilement une tâche et l'assigner/traiter plus tard quand le temps le permet.

# Mockup

![./mockup](./docs/mockup.png)

# Data model

![./datamodel](./docs/data-model.png)

# Language et lib

- SpringBoot + Postgres + 
- Angular + ngx-graph + ngx-markdown + ngx-drag-drop


# Objectifs premier

- Backend spring boot
	- CRUD projet
	- CRUD tâches
	- Opération sur backlog de tâches

- Frontend Angular
	- CRUD projet
	- CRUD tâches
	- Modélisation des data
	- interface avec le backend

# Améliorations

- Backend
	- CRUD user (signup/login avec des user dans la DB)
	- login avec token jwt
	- Synchronization des tâches avec gitlab
	- Synchronization des tâches avec github

- Frontend
	- Editeur markdown pour les tâches
	- Visualisation en graph
	- Drag n Drop

# Changelog

Définition du scope de l'application - 20.2.24

Pour aller de l'avant dans le projet il est important de noté que le scope sur lequel s'effectuera les opérations de création/edition/suppression dans un premier temps seront globale.

C'est à dire qu'aucune sécurité ne sera mise en place pour limiter les actions utilisateurs sur des entité qu'il ne leurs "appartiendrait" pas.


# Démarrer le projet

Pour démarrer le projet il faut lancer plusieurs chose:
- avoir une DB postgres
- modifié les accès en cas de besoin dans le backend
- lancer le backend
- lancer le frontend
- se loger

## Db postgres

Il existe un script `./Backend/db.sh` qui lancera une base de donnée sur **docker**.

Si vous ne posséder pas docker, il vous faudra trouver un moyen de lancer une base postgres sur votre machine.


## Modifié les accès DB

les accès à la DB se trouve dans ce fichier `./Backend/src/main/resources/application.properties`.

Ce fichier vous permettra de changer les accès à la DB en conséquence.

## Lancer le backend

Il vous faudra ouvrir le projet `Backend` dans **intellij** et lancer le projet.

Le développement s'est fait sous `OpenJDK - v21.0.1`.

Dès que le projet est lancé, le backend est disponible à `localhost:8090`

## Lancer le frontend

Pour lancer le frontend il vous faudra visual studio.

Il vous faudra aussi `nodejs v18`.

Dès que le dépot est cloner, il vous faudra lancer dans un **terminal** les commandes suivante:

```bash
npm i -g @angular/cli
cd <depot>/Frontend
npm i

# Lancer l'application

npm run dev
```

Cela lancera le frontend sur le port `locahost:4200` (votre navigateur devrait se lancer automatiquement...)


## Se loger

Il existe un utilisateur par défault qui est créer au démarrage de l'application

username: `admin`

mdp: `admin`

Cette utilisateur devrait déjà posséder un projet et quelque tâches.

# Tour de l'application

## Home page

![./home](./docs/home.png)

## Login

![./login](./docs/login.png)

## Signup

![./signup](./docs/signup.png)

## Dashboard

![./dashboard](./docs/dashboard.png)

## projects

![./projects](./docs/projects.png)

## project-detail

![./project-detail](./docs/project-detail.png)

## tasks

![./tasks](./docs/tasks.png)

## task-detail

![./task-detail](./docs/task-detail.png)

