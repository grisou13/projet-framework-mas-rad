package org.hearc.backend.Auth;

public enum RoleEnum {
    USER("USER"),
    ADMIN("ADMIN");

    private final String role_name;
    //Constructor to define name
    RoleEnum(String role_name) {
        this.role_name = role_name;
    }
    //override the inherited method
    @Override
    public String toString() {
        return role_name;
    }
}
