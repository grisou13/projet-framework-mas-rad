package org.hearc.backend.Dtos;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Convert;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hearc.backend.Entities.CustomEntityAttribute;
import org.hearc.backend.Entities.ProjectType;
import org.hearc.backend.Entities.Task;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class ProjectCreate
{
    private String title;
    private Date deadline;
    private ProjectType type = ProjectType.PRO;
    private List<TaskCreate> tasks = new ArrayList<>();

    @Convert(converter = AttributeConverter.class)
    private List<CustomEntityAttribute> attributes;

}
