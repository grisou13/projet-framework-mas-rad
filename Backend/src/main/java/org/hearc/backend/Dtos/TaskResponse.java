package org.hearc.backend.Dtos;

import jakarta.annotation.Nullable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hearc.backend.Entities.CustomEntityAttribute;
import org.hearc.backend.Entities.Task;
import org.hearc.backend.Entities.TaskType;
import org.hearc.backend.Entities.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TaskResponse {
    private Long id;
    private String content;
    private Date deadline;
    private TaskType type;
    private Long projectId;
    private Long responsibleId;
    private Long parentId;
    private Boolean done;
    private Date doneAt;

    private CustomEntityAttribute[] attributes = {};


//    public static TaskResponse fromEntity(Task task)
//    {
//        return new TaskResponse(task.getId(), task.getTitle(), task.getResponsible());
//    }
}
