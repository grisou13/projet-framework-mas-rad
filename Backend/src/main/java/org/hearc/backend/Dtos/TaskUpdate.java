package org.hearc.backend.Dtos;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Convert;
import lombok.Getter;
import lombok.Setter;
import org.hearc.backend.Entities.CustomEntityAttribute;
import org.hearc.backend.Entities.Project;
import org.hearc.backend.Entities.TaskType;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
public class TaskUpdate {
    private String content;
    private Date deadline;
    private TaskType type = TaskType.TASK;

    private Boolean done;
    private Date doneAt;

    @Convert(converter = AttributeConverter.class)
    private CustomEntityAttribute[] attributes;

    private Long projectId;
    private Long parentId;
    private Long responsibleId;
}
