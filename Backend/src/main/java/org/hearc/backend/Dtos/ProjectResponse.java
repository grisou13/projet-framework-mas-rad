package org.hearc.backend.Dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hearc.backend.Entities.CustomEntityAttribute;
import org.hearc.backend.Entities.Project;
import org.hearc.backend.Entities.ProjectType;
import org.hearc.backend.Entities.Task;
import org.springframework.lang.Nullable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProjectResponse {
    private Long id;
    private String title;
    private ProjectType type;
    @Nullable
    private Date deadline;
    private List<TaskResponse> tasks = new ArrayList<>();

    private CustomEntityAttribute[] attributes = {};
//    public static ProjectResponse fromEntity(Project project) {
//        return new ProjectResponse(
//            project.getId(),
//            project.getTitle(),
//            project.getTasks().stream().map(TaskResponse::fromEntity).toList()
//        );
//    }

}
