package org.hearc.backend.Dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class HealthCheckResponse {
    private String Time;
    private HealthStatus Status;
}
