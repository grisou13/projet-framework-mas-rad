package org.hearc.backend.Dtos;

public enum HealthStatus {
    OK("OK"),
    UNHEALTHY("UNHEALTHY");

    private final String healthName;
    //Constructor to define name
    HealthStatus(String healthName) {
        this.healthName = healthName;
    }
    //override the inherited method
    @Override
    public String toString() {
        return healthName;
    }
}
