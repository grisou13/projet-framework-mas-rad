package org.hearc.backend.Dtos;

import lombok.Getter;
import lombok.Setter;
import org.hearc.backend.Auth.RoleEnum;

import java.util.List;

@Getter
@Setter
public class UserResponse {
    private String username;
    private List<RoleEnum> roles;
}
