package org.hearc.backend.Dtos;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Convert;
import lombok.Getter;
import lombok.Setter;
import org.hearc.backend.Entities.CustomEntityAttribute;
import org.hearc.backend.Entities.TaskType;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class TaskCreate {
    private String content;
    private Date deadline;
    private TaskType type = TaskType.TASK;
    private Boolean done = false;
    private Date doneAt;
    
    @Convert(converter = AttributeConverter.class)
    private List<CustomEntityAttribute> attributes;

    private Long projectId;
    private Long parentId;
    private Long responsibleId;
}
