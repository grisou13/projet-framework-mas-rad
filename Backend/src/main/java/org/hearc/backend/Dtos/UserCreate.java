package org.hearc.backend.Dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCreate {
    private String username;
    private String password;
}
