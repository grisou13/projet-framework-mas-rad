package org.hearc.backend.Services;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.hearc.backend.Auth.AuthenticatedUser;
import org.hearc.backend.Auth.RoleEnum;
import org.hearc.backend.Dtos.UserCreate;
import org.hearc.backend.Entities.User;
import org.hearc.backend.Repositories.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
@Log4j2
public class UserService  implements UserDetailsService {

    private final UserRepository repository;
    public Optional<User> registerNewUserAccount(UserCreate accountDto, PasswordEncoder encoder){
        if (this.repository.existsByUsername(accountDto.getUsername())) {
            return Optional.empty();
        }
        User user = new User();
        user.setPassword(encoder.encode(accountDto.getPassword()));
        user.setUsername(accountDto.getUsername());
        user.setRoles(List.of(RoleEnum.USER));
        return Optional.of(repository.save(user));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = repository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Cannot found user"));
        log.info("Loading in user with details {}", user);
        return new AuthenticatedUser(user);
    }
}
