package org.hearc.backend.Services;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.hearc.backend.Auth.AuthenticatedUser;
import org.hearc.backend.Dtos.ProjectCreate;
import org.hearc.backend.Dtos.ProjectResponse;
import org.hearc.backend.Dtos.ProjectUpdate;
import org.hearc.backend.Dtos.TaskResponse;
import org.hearc.backend.Entities.Project;
import org.hearc.backend.Entities.Task;
import org.hearc.backend.Repositories.ProjectRepository;
import org.hearc.backend.Repositories.TaskRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class ProjectService {
    private final ProjectRepository repository;
    private final TaskRepository taskRepository;
    private final ModelMapper mapper = new ModelMapper();

    public Optional<ProjectResponse> Create(ProjectCreate request)
    {
        var authenticatedUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var newEntity = mapper.map(request, Project.class);
        newEntity.setLeader(authenticatedUser.getUser());
        var entity  = this.repository.save(newEntity);
        var response = new ProjectResponse(entity.getId(), entity.getTitle(), entity.getType(), entity.getDeadline(), new ArrayList<>(), entity.getAttributes());
        if(request.getTasks().isEmpty()){
            return Optional.of(response);
        }

        //assign tasks to the new project
        entity.setTasks(new ArrayList<>()); //empty the tasks first otherwise save won't work....

        var tasks = this.taskRepository.saveAll(request.getTasks().stream().map(x -> {
            var taskEntity = mapper.map(x, Task.class);
            taskEntity.setProject(entity);
            return taskEntity;
        }).toList());

        response.setTasks(tasks.stream().map(x -> this.mapper.map(x, TaskResponse.class)).toList());
        return Optional.of(response);
    }

    public Optional<ProjectResponse> update(ProjectUpdate request, Long id)
    {
        var entity = this.repository.findById(id);
        if(entity.isEmpty()){
            return Optional.empty();
        }

//        var data = mapper.map(request, Project.class);
        var updatePayload = entity.get();
        updatePayload.setDeadline(request.getDeadline());
        updatePayload.setTitle(request.getTitle());
        updatePayload.setType(request.getType());

        //TODO add leader
//        updatePayload.setLeader(request.getLeader());
        //TODO define if we should or can do this in spring
        //updatePayload.setTasks(data.getTasks());
        return Optional.of(mapper.map(this.repository.save(updatePayload), ProjectResponse.class));
    }

    public List<ProjectResponse> getAll()
    {
        var authenticatedUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return this.repository.findForUser(authenticatedUser.getUser().getId()).stream().map(x -> mapper.map(x, ProjectResponse.class)).toList();
    }

    public Optional<ProjectResponse> getSingle(@RequestParam Long id)
    {
        var authenticatedUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        var entity = this.repository.findByUserAndId(id, authenticatedUser.getUser().getId());
        if(entity.isEmpty()){
            return Optional.empty();
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return Optional.of(mapper.map(entity.get(), ProjectResponse.class));
    }

    public Optional<Boolean> deleteSingle(Long id) {
        if(!this.repository.existsById(id))
        {
            return Optional.empty();
        }
        this.repository.deleteById(id);
        return Optional.of(true);
    }
}
