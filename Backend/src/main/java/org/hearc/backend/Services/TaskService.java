package org.hearc.backend.Services;

import jakarta.transaction.Transactional;
import org.hearc.backend.Auth.AuthenticatedUser;
import org.hearc.backend.Dtos.TaskCreate;
import org.hearc.backend.Dtos.TaskResponse;
import org.hearc.backend.Dtos.TaskUpdate;
import org.hearc.backend.Entities.BaseEntity;
import org.hearc.backend.Entities.Task;
import org.hearc.backend.Repositories.ProjectRepository;
import org.hearc.backend.Repositories.TaskRepository;
import org.hearc.backend.Repositories.UserRepository;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional

public class TaskService {
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;
    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    public TaskService(TaskRepository taskRepository, ProjectRepository projectRepository, UserRepository userRepository){
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;

        this.mapper.getConfiguration()
                .setAmbiguityIgnored(true)
                .setMatchingStrategy(MatchingStrategies.STRICT)
                .setSkipNullEnabled(true);

//        mapper.typeMap(TaskCreate.class, BaseEntity.class)
//                .addMappings(mapper -> mapper.skip(BaseEntity::setId));
        mapper
                .typeMap(TaskCreate.class, Task.class)
                .addMappings(mapper -> {
                    mapper.skip(Task::setId);
                    mapper.skip(Task::setParent);
                    mapper.skip(Task::setProject);
                    mapper.skip(Task::setResponsible);
//                    mapper.map(TaskCreate::getContent, Task::setTitle);
//                    mapper.map(TaskCreate::getDeadline, Task::setDeadline);
                });
        mapper
                .typeMap(TaskUpdate.class, Task.class)
                .addMappings(mapper -> {
                    mapper.skip(Task::setId);
                    mapper.skip(Task::setParent);
                    mapper.skip(Task::setProject);
                    mapper.skip(Task::setResponsible);
//                    mapper.map(TaskUpdate::getTitle, Task::setTitle);
//                    mapper.map(TaskUpdate::getDeadline, Task::setDeadline);
                });
        mapper.typeMap(Task.class, TaskResponse.class)
                .addMappings(mapper -> {

                    mapper.map(s -> {
                        assert s.getParent() != null;
                        return s.getParent().getId();
                    }, TaskResponse::setParentId);
                    mapper.map(s -> {
                        assert s.getResponsible() != null;
                        return s.getResponsible().getId();
                    }, TaskResponse::setResponsibleId);
                    mapper.map(s -> {
                        assert s.getProject() != null;
                        return s.getProject().getId();
                    }, TaskResponse::setProjectId);
                });
//        mapper
//                .createTypeMap(TaskUpdate.class, Task.class)
//                .addMappings(mapper -> mapper.skip(Task::setId))
//                .addMappings(mapper -> mapper.skip(Task::setParent))
//                .addMappings(mapper -> mapper.skip(Task::setProject))
//                .addMappings(mapper -> mapper.skip(Task::setResponsible));

        /*mapper.addMappings(new PropertyMap<TaskUpdate, Task>() {
            protected void configure() {
                skip().setParent(null);
                skip().setProject(null);
                skip().setResponsible(null);
            }
        });*/
    }

    public Optional<TaskResponse> Create(TaskCreate payload)
    {
        var entity = mapper.map(payload, Task.class);

        if(payload.getProjectId() != null){
            var project = this.projectRepository.findById(payload.getProjectId());
            if(project.isEmpty()){
                return Optional.empty();
            }
            entity.setProject(project.get());
        }
        var authenticatedUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var responsible = authenticatedUser.getUser();

        if(payload.getResponsibleId() != null){
            var searchedUser = this.userRepository.findById(payload.getParentId());
            if(searchedUser.isEmpty()){
                return Optional.empty();
            }
            responsible = searchedUser.get();
        }
        entity.setResponsible(responsible);

        if(payload.getParentId() != null){
            var parentTask = this.taskRepository.findById(payload.getParentId());
            if(parentTask.isEmpty()){
                return Optional.empty();
            }
            entity.setParent(parentTask.get());
        }

        entity = this.taskRepository.save(entity);
        var response = mapper.map(entity, TaskResponse.class);

        return Optional.of(response);
    }

    public Optional<TaskResponse> Update(Long id, TaskUpdate payload) {
        var entity = this.taskRepository.findById(id);
        if(entity.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        var updatedEntity = entity.get();

        updatedEntity.setContent(payload.getContent());
        updatedEntity.setDeadline(payload.getDeadline());
        updatedEntity.setType(payload.getType());
        updatedEntity.setAttributes(payload.getAttributes());
        updatedEntity.setDone(payload.getDone());
        updatedEntity.setDoneAt(payload.getDoneAt());
//        updatedEntity.setTitle(payload.getTitle());
        if(payload.getDone() != null && payload.getDone() && payload.getDoneAt() == null){
            updatedEntity.setDoneAt(new Date());
        }
        if(payload.getProjectId() != null){
            var project = this.projectRepository.findById(payload.getProjectId());
            if(project.isEmpty()){
                return Optional.empty();
            }
            updatedEntity.setProject(project.get());
        }
        var authenticatedUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var responsible = authenticatedUser.getUser();

        if(payload.getResponsibleId() != null){
            var searchedUser = this.userRepository.findById(payload.getResponsibleId());
            if(searchedUser.isEmpty()){
                return Optional.empty();
            }
            responsible = searchedUser.get();
        }
        updatedEntity.setResponsible(responsible);

        if(payload.getParentId()!= null){
            var parentTask = this.taskRepository.findById(payload.getParentId());
            if(parentTask.isEmpty()){
                return Optional.empty();
            }
            updatedEntity.setParent(parentTask.get());
        }

        var response = mapper.map(this.taskRepository.save(updatedEntity), TaskResponse.class);
        return Optional.of(response);
    }

    public List<TaskResponse> GetAll() {
        //TODO scope all tasks to user? - not for now, but should be done in the futur of the project
        var authenticatedUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return this.taskRepository.findForResponsible(authenticatedUser.getUser().getId()).stream().map(x -> mapper.map(x, TaskResponse.class)).toList();
    }

    public Optional<TaskResponse> getSingle(Long id) {
        var authenticatedUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        var entity = this.taskRepository.findByUserAndId(id, authenticatedUser.getUser().getId());
        if(entity.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(mapper.map(entity.get(), TaskResponse.class));
    }

    public Optional<Boolean> deleteSingle(Long id)
    {
        if(!this.taskRepository.existsById(id))
        {
            return Optional.empty();
        }
        this.taskRepository.deleteById(id);
        return Optional.of(true);
    }

    public List<TaskResponse> getForProject(Long projectId) {
        var authenticatedUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return this.taskRepository.findForProjectAndUser(projectId, authenticatedUser.getUser().getId())
                .stream().map(x -> mapper.map(x, TaskResponse.class)).toList();
    }
}
