package org.hearc.backend.Services;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.hearc.backend.Auth.RoleEnum;
import org.hearc.backend.Entities.*;
import org.hearc.backend.Repositories.ProjectRepository;
import org.hearc.backend.Repositories.TaskRepository;
import org.hearc.backend.Repositories.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

@Component
@Log4j2
@AllArgsConstructor
public class BootService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    /**
     * Init first user for login
     */
    public void seedDatabase() {
        String username = "admin";
        userRepository.findByUsername(username).orElseGet(() -> {

            User user = new User();
            user.setId(100000L);
            user.setUsername(username);
            user.setPassword(passwordEncoder.encode("admin"));
            user.setRoles(Arrays.asList(RoleEnum.ADMIN, RoleEnum.USER));
            userRepository.save(user);

            log.info("Created root user: {}", user.getId());


            var prjectTask = new Task();
            prjectTask.setType(TaskType.TASK);
            prjectTask.setContent("My projects first task");
            prjectTask.setResponsible(user);

            var taskList = new ArrayList<Task>();
            taskList.add(prjectTask);
            var project = new Project();
            project.setId(100000L);
            project.setTitle("Initial project");
            project.setType(ProjectType.PERSONAL);
            project.setLeader(user);
            project.setTasks(taskList);

            projectRepository.save(project);

            var task = new Task();
            task.setContent("My first backlog task");
            prjectTask.setType(TaskType.BACKLOG);
            task.setResponsible(user);
            task.setDone(false);

            taskRepository.save(task);



            return user;
        });

    }
}
