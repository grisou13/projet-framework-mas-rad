package org.hearc.backend.Repositories;

import org.hearc.backend.Entities.Project;
import org.hearc.backend.Entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    @Query("select p from Project p where p.id = :id and p.leader.id = :userId")
    public Optional<Project> findByUserAndId(@Param("id") Long id, @Param("userId") Long userId );

    @Query("select p from Project p where p.leader.id = :userId")
    public List<Project> findForUser( @Param("userId") Long userId );
}