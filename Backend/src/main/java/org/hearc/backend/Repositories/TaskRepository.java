package org.hearc.backend.Repositories;

import org.hearc.backend.Entities.Project;
import org.hearc.backend.Entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    @Query("select t from Task t where t.id = :id and t.responsible.id = :userId")
    public Optional<Task> findByUserAndId(@Param("id") Long id, @Param("userId") Long userId );

    @Query("select t from Task t where t.responsible.id = :userId")
    public List<Task> findForResponsible(@Param("userId") Long userId );

    @Query("select t from Task t where t.responsible.id = :userId and t.project.id = :projectId")
    public List<Task> findForProjectAndUser(@Param("projectId") Long projectId, @Param("userId") Long userId);
}