package org.hearc.backend.Entities;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import jakarta.persistence.AttributeConverter;
import lombok.Getter;
import lombok.Setter;
import org.hearc.backend.Auth.RoleEnum;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Entity
@Getter
@Setter
@Table(name = "tasks")
public class Task extends BaseEntity{
    private String content;
    private Date deadline;
    private TaskType type = TaskType.BACKLOG;
    @Nullable
    private Boolean done = false;
    @Nullable
    private Date doneAt;

    @Column(name = "attributes")
    @Convert(converter = CustomAttributeConverter.class)
    private CustomEntityAttribute[] attributes;

    @OneToOne
    @Nullable
    private Task parent;

    @ManyToOne()
    @JoinColumn(name="responsible_id")
    private User responsible;

    @ManyToOne(optional = true)
    @JoinColumn(name="project_id", nullable = true)
    @Nullable
    private Project project;
}
