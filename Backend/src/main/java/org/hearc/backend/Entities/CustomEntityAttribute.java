package org.hearc.backend.Entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomEntityAttribute
{
    private String key;
    private String value;
    
}
