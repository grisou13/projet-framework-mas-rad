package org.hearc.backend.Entities;

import jakarta.persistence.*;
import jakarta.persistence.AttributeConverter;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Setter
@Getter
@Table(name = "projects")
public class Project extends BaseEntity{


    private String title;
    private Date deadline;
    private ProjectType type = ProjectType.PRO;

    @Column(name = "attributes")
    @Convert(converter = CustomAttributeConverter.class)
    private CustomEntityAttribute[] attributes;

    @OneToMany(mappedBy = "project",cascade = CascadeType.REMOVE)
    private List<Task> Tasks = new ArrayList<>();

    @ManyToOne
    private User leader;

}
