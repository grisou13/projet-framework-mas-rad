package org.hearc.backend.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hearc.backend.Auth.RoleEnum;
import org.hibernate.type.descriptor.converter.internal.CollectionConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "users")
public class User extends BaseEntity {
    @Column(unique = true)
    private String username;

    private String password;

    @Column(name = "role")
    private String roles;
    public List<RoleEnum> getRoles()
    {
        return Arrays.stream(this.roles.split(",")).map(x -> RoleEnum.valueOf(x)).toList();
    }
    public void setRoles(List<RoleEnum> val)
    {
        this.roles = String.join(",", val.stream().map(x -> x.toString()).toList());
    }
    public void setRoles(String val)
    {
        this.roles = val;
    }
    //This relationship defines the tasks a user is responsible for. This allows assignable tasks to a different user than the project lead.
    @OneToMany
    private List<Task> LiableTasks = new ArrayList<>();

    @OneToMany(mappedBy = "leader", cascade = CascadeType.REMOVE)
    private List<Project> Projects = new ArrayList<>();


}
