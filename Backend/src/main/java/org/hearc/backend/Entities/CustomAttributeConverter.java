package org.hearc.backend.Entities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import lombok.extern.java.Log;

@Converter(autoApply=true)
@Log

public class CustomAttributeConverter implements AttributeConverter<CustomEntityAttribute[], String> {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(CustomEntityAttribute[] attributeList) {
        try {
            return objectMapper.writeValueAsString(attributeList);
        } catch (JsonProcessingException jpe) {
            log.warning("Cannot convert CustomAttribute[] into JSON");
            return null;
        }
    }

    @Override
    public CustomEntityAttribute[] convertToEntityAttribute(String value) {
        try {
            return objectMapper.readValue(value, CustomEntityAttribute[].class);
        } catch (JsonProcessingException e) {
            log.warning("Cannot convert JSON into CustomAttribute[]");
            return null;
        }
    }

}
