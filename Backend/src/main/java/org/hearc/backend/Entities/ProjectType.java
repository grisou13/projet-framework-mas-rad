package org.hearc.backend.Entities;

public enum ProjectType
{
    PERSONAL("PERSONAL"),
    PRO("PRO");

    private final String project_type;
    //Constructor to define name
    ProjectType(String project_type) {
        this.project_type = project_type;
    }
    //override the inherited method
    @Override
    public String toString() {
        return project_type;
    }
}
