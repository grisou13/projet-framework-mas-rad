package org.hearc.backend.Entities;

public enum TaskType
{
    TASK("TASK"),
    ISSUE("ISSUE"),
    BACKLOG("BACKLOG");

    private final String task_type;
    //Constructor to define name
    TaskType(String task_type) {
        this.task_type = task_type;
    }
    //override the inherited method
    @Override
    public String toString() {
        return task_type;
    }
}
