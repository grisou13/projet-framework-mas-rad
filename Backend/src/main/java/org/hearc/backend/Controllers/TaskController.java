package org.hearc.backend.Controllers;

import lombok.AllArgsConstructor;
import org.hearc.backend.Dtos.TaskCreate;
import org.hearc.backend.Dtos.TaskResponse;
import org.hearc.backend.Dtos.TaskUpdate;
import org.hearc.backend.Services.TaskService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("tasks")
@AllArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    public TaskResponse create(@RequestBody TaskCreate request) {
        return this.taskService.Create(request).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
    }

    @PutMapping(value = "{id}", consumes = "application/json", produces = "application/json")
    public TaskResponse update(@PathVariable Long id, @RequestBody TaskUpdate request) {
        return this.taskService.Update(id, request).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));

    }

    @GetMapping(value = "", produces = "application/json")
    public List<TaskResponse> getAll(@RequestParam(required = false) Long projectId) {
        if(projectId == null){
            return this.taskService.GetAll();
        }
        return this.taskService.getForProject(projectId);
    }

    @GetMapping(value = "{id}", produces = "application/json")
    public TaskResponse getSingle(@PathVariable Long id) {
        return this.taskService.getSingle(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping(value = "{id}", produces = "application/json")
    public Boolean deleteSingle(@PathVariable Long id) {
        return this.taskService.deleteSingle(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}