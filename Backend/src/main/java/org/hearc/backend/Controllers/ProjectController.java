package org.hearc.backend.Controllers;

import lombok.AllArgsConstructor;
import org.hearc.backend.Dtos.ProjectCreate;
import org.hearc.backend.Dtos.ProjectResponse;
import org.hearc.backend.Dtos.ProjectUpdate;
import org.hearc.backend.Services.ProjectService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("projects")
@AllArgsConstructor
public class ProjectController {
    private final ProjectService projectService;

    @PostMapping(value = "", consumes =  "application/json", produces = "application/json")
    public ProjectResponse create(@RequestBody ProjectCreate request)
    {
        return this.projectService.Create(request).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
    }

    @PutMapping(value = "{id}", consumes =  "application/json", produces = "application/json")
    public ProjectResponse update(@RequestBody ProjectUpdate request, @PathVariable Long id)
    {
        return this.projectService.update(request, id).orElseThrow(() -> new ResponseStatusException((HttpStatus.NOT_FOUND)));
    }

    @GetMapping(value = "", produces = "application/json")
    public List<ProjectResponse> getAll()
    {
        return this.projectService.getAll();
    }

    @GetMapping(value = "{id}", produces = "application/json")
    public ProjectResponse getSingle(@PathVariable Long id)
    {
        return this.projectService.getSingle(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping(value = "{id}", produces = "application/json")
    public Boolean deleteSingle(@PathVariable Long id)
    {
        return this.projectService.deleteSingle(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
