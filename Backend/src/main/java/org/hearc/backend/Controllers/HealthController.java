package org.hearc.backend.Controllers;

import org.hearc.backend.Dtos.HealthCheckResponse;
import org.hearc.backend.Dtos.HealthStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("healthz")
public class HealthController {
    @GetMapping()
    public HealthCheckResponse Get()
    {
        return new HealthCheckResponse(LocalDateTime.now().toString(), HealthStatus.OK);
    }
}
