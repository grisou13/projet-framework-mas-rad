package org.hearc.backend.Controllers;

import lombok.AllArgsConstructor;
import org.hearc.backend.Auth.AuthenticatedUser;
import org.hearc.backend.Dtos.UserCreate;
import org.hearc.backend.Dtos.UserResponse;
import org.hearc.backend.Services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("auth")
@AllArgsConstructor
public class AuthController {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper mapper = new ModelMapper();

    @PostMapping(value = "register")
    public UserResponse Register(@RequestBody UserCreate request)
    {

        var result = userService.registerNewUserAccount(request, passwordEncoder);
        if(result.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        return mapper.map(result.get(), UserResponse.class);
    }
    // This endpoint doesn't do much for now, but it will be usefull later
    @PostMapping(value = "login")
    public UserResponse Login(@RequestBody  UserCreate request)
    {

        var result = (AuthenticatedUser) userService.loadUserByUsername(request.getUsername());
        if(result == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if(!passwordEncoder.matches(request.getPassword(), result.getPassword() )){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return mapper.map(result.getUser(), UserResponse.class);
    }
    @GetMapping(value = "me")
    public UserResponse Me()
    {
        var user = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return mapper.map(user.getUser(), UserResponse.class);
    }
}
