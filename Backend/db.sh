#!/bin/bash

docker rm postgres &> /dev/null

docker run  --name postgres -e POSTGRES_PASSWORD=1234 -e POSTGRES_DB=springboot -d -v postgres:/var/lib/postgresql -p 5432:5432 postgres
#docker exec -e POSTGRES_PWD=1234 -it postgres "/usr/bin/psql -u postgres -W < 'create database springboot;'"