import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { map, of, switchMap } from 'rxjs';
import { ProjectsService } from '../services/projects.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  templateUrl: './project-layout.component.html',
  styleUrls: [],
})
export class ProjectLayoutComponent implements OnInit {
  title: string | undefined = '';
  route = inject(ActivatedRoute);
  router = inject(Router);
  projectService = inject(ProjectsService);


  projects$ = this.router.routerState.root.paramMap.pipe(
    switchMap(params => {
      const projectId = params.get("projectId");
      console.log("Projectid ", projectId);

      if(!projectId){
        return of([]);
      }

      return this.projectService.getSingle(projectId).pipe(map( v => ([v])))
    }),
    takeUntilDestroyed()
  )
    
  ngOnInit() {
    this.title = this.route.firstChild?.snapshot.title || '';
  }
}
