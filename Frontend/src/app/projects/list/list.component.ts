import { Component, inject } from '@angular/core';
import { ProjectsService } from '../services/projects.service';
import { BehaviorSubject, Subject, switchMap, tap } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Project } from '../schema';
import { BadgeColor } from '@shared/components/status-badge/status-badge.component';

@Component({
  templateUrl: './list.component.html'
})
export class ListComponent {
  projectService = inject(ProjectsService)

  loadable = new BehaviorSubject<null>(null);
  deletable = new Subject<string>();

  loadable$ = this.loadable.pipe(switchMap( (_) => this.projectService.getAll()))

  deletable$ = this.deletable
    .pipe(
      switchMap( (id) => this.projectService.delete(id)),
      tap(x => {
        this.loadable.next(null)
      }),
      takeUntilDestroyed()
  )
  .subscribe();

  deleteProject(projectId: string){
    this.deletable.next(projectId);
  }
  projectBadge(project: Project){
    if(project.tasks.every(x => x.done)) return BadgeColor.Green;
    const now = new Date();
    if(project.deadline && project.deadline >= now) return BadgeColor.Red;

    return BadgeColor.Dark;
  }

  projectStatus(project: Project){
    if(project.tasks.every(x => x.done)) return "Done";
    const now = new Date();
    if(project.deadline && project.deadline >= now) return "Late";
    
    return "On going";
  }
}
