import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Project, ProjectTypeMapping, UpsertProject, attributeForm, projectForm } from '../schema';
import { FormArray, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { BehaviorSubject, EMPTY, map, of, switchMap, tap } from 'rxjs';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, ActivatedRouteSnapshot, ParamMap } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ProjectsService } from '../services/projects.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { SharedModule } from "../../shared/shared.module";
import { taskForm } from 'src/app/tasks/schema';
import { handleSuccess } from '@shared/extensions/handleSuccessRx.pipe';
import { handleAppError } from '@shared/extensions/handleErrorRx.pipe';

@Component({
    standalone: true,
    templateUrl: './form.component.html',
    selector: "app-project-form",
    imports: [ReactiveFormsModule, CommonModule, SharedModule]
})
export class ProjectForm {
  @Input() projectId : string | null = null;
  projectType = ProjectTypeMapping;
  create$ = this.route.paramMap.pipe(
    switchMap(params => 
      this.projectService.create(this.form.value as unknown as UpsertProject)
    ),
    handleSuccess((val) => `Project ${(val as Project).id} created`),
    handleAppError((err) => {
      return {
        message: "We could not create project ",
        status: "",
        context: "project",
        error: err
      }
    })
  )
  update$ = this.route.paramMap.pipe(
    switchMap(params => 
      this.projectService.updateSingle(params.get('projectId')!, this.form.value as unknown as UpsertProject)
    ),
    handleSuccess((val) => `Project ${(val as Project).id} updated`),
    handleAppError((err) => {
      return {
        message: "We could not update project ",
        status: "",
        context: "project",
        error: err
      }
    })
  )
  // @Output() save = new EventEmitter<typeof this.form.value>();
  saveable$ = this.route.paramMap.pipe(
    switchMap(params => 
      params.has('projectId') ? 
      this.update$ : 
      this.create$
    ),

    takeUntilDestroyed()
  );
  loading$ = new BehaviorSubject(false);
  form = projectForm();

  invalid$ = this.form.statusChanges.pipe(map(x => x === 'INVALID'));
  
  constructor(private route: ActivatedRoute, private projectService: ProjectsService) { }

  loadable$ = this.route.paramMap.pipe(map(params => {
    return this.projectId != null ? this.projectId : params.get('projectId')
  }), switchMap( id => {
    return id === null ? of(null) : this.projectService.getSingle(id)
  }),
  tap({
    next: (val) => {
      this.bootstrapFormValue(val);
    }
  }) )


  protected bootstrapFormValue(data: Project | null)
  {
    if(data === null){
      return;
    }
    
    this.form.patchValue({
      title: data.title,
      deadline: data?.deadline ?? undefined,
      type: data.type,
    });
    
    data.tasks.forEach(x =>{
      const control = taskForm();
      control.patchValue( {
        id: x.id,
        content: x.content,
        deadline: x.deadline ?? undefined,
        type: x.type,
        parentId: x?.parentId ?? undefined,
        responsibleId: x?.responsibleId ?? undefined,
        projectId: x?.projectId ?? data?.id ?? undefined,
      });
      (this.form.get('tasks') as FormArray)?.push(control)
    });
    data.attributes?.forEach(x => {
      const control = attributeForm();

      control.patchValue({key: x.key, value: x.value});

      (this.form.get('attributes') as FormArray)?.push(control)

    });

    // (this.form.get('tasks') as FormArray)?.push(taskValue);
    // (this.form.get('attributes') as FormArray)?.push(attributes);
    
  }
  submit() {
    if(this.form.invalid){
      return;
    }
    this.saveable$.subscribe();
    // this.save.emit(this.form.value);
  }

  getControl(name: string){
    return this.form.get(name) as FormControl;
  }

  formStatus() {
    return Object.keys(this.form.controls).map(k => `${k}: ${this.form.get(k)?.status}`).join(' - ')
  }
}
