import { Component, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, of, switchMap } from 'rxjs';
import { ProjectsService } from '../services/projects.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  templateUrl: './edit.component.html'
})
export class EditComponent {
  
  title: string | undefined = '';
  route = inject(ActivatedRoute);
  router = inject(Router);
  projectService = inject(ProjectsService);

  projectId$ = this.route.paramMap.pipe(
    map(params => {
      const projectId = params.get("projectId");
      console.log("Projectid ", projectId);
      return projectId;
    })
  )
  projects$ = this.projectId$.pipe(
    switchMap(projectId => {
      if(!projectId){
        return of([]);
      }

      return this.projectService.getSingle(projectId).pipe(map( v => ([v])))
    }),
    takeUntilDestroyed()
  )

  onClickGraph($event: { type: string; id: string; }) {
    switch($event.type.toUpperCase()){
      case "PROJECT":
        this.router.navigate(["/projects/"+$event.id], {relativeTo: this.route});
        break;
      case "TASK":
        this.router.navigate(["/tasks/"+$event.id], {relativeTo: this.route})
        break;
      default:
        break;
    }
  }
}
