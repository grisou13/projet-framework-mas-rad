import { Injectable, inject } from '@angular/core';


import { HttpClient } from '@angular/common/http';
import { Project, UpsertProject, parseProject, parseProjectList, schema, validator } from '../schema';
import { concatAll, from, map, mergeAll, of, switchMap, toArray } from 'rxjs';



@Injectable({
  providedIn: 'root',
})
export class ProjectsService {
  
  http = inject(HttpClient);
  getAll = () => this.http.get<Project[]>('/projects').pipe(parseProjectList)
  getSingle = (id: string) => this.http.get<Project>('/projects/'+id).pipe(parseProject)
  updateSingle = (id: string, payload: UpsertProject) => from(validator.parseAsync(payload)).pipe(switchMap( p => this.http.put('/projects/'+id, p)));
  create = (payload: UpsertProject) =>  from(validator.parseAsync(payload)).pipe(switchMap( p => this.http.post('/projects', p)));
  delete = (id: string) => this.http.delete('/projects/'+id)
  
}
