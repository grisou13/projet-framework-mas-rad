import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectLayoutComponent } from './layout/project-layout.component';
import { ListComponent } from './list/list.component';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { routes } from './projects.routes';
import { ProjectsService } from './services/projects.service';
import { TasksModule } from '../tasks/tasks.module';
import { InputComponent } from '@shared/components/input/input.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { GraphVizComponent } from '@shared/components/graph-viz/graph-viz.component';
import { EditComponent } from './edit/edit.component';
import { ProjectForm } from './form/form.component';
import { NewTaskComponent } from '../tasks/components/new-task/new-task.component';



@NgModule({
  declarations: [ProjectLayoutComponent, ListComponent, EditComponent ],
  providers: [
    ProjectsService
  ],
  imports: [SharedModule, ProjectForm, GraphVizComponent, SweetAlert2Module.forChild(), RouterModule.forChild(routes), NewTaskComponent],
  exports: [RouterModule],
})
export class ProjectsModule {
  static forRoot(): ModuleWithProviders<ProjectsModule> {
    return {
      ngModule: ProjectsModule,
      providers: [
        ProjectsService
      ],
    };
  }
}
