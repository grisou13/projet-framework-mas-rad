import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { z } from 'zod';
import { deadlineNonRequire, taskForm, schema as taskSchema, validator as taskValidator } from '../tasks/schema';
import { validator as customAttributeValidator} from '@shared/schema';
import { map } from 'rxjs';



export const validator = z.object({
    title: z.string().min(3).max(25),
    deadline: z.coerce.date().nullable().optional(), //z.string().transform((str) => new Date(str ?? '')).optional().nullable(), // z.coerce.date(),
    type: z.union([z.literal('PRO'), z.literal('PERSONAL')]).default('PRO'),
    tasks: z.array(taskValidator),
    attributes: z.array(customAttributeValidator).nullable()
});

export const schema = z
.object({
    id: z.number(),
    tasks: z.array(taskSchema)
}).merge(validator.omit({tasks: true}));


export type UpsertProject = z.infer<typeof validator>;

export const attributeForm = () => new FormGroup({
  key: new FormControl(''),
  value: new FormControl('')
})

export type Project = z.infer<typeof schema>;
export const projectForm = () =>  new FormGroup({
        title: new FormControl('', [
          Validators.minLength(3),
          Validators.required,
        ]),
        deadline: new FormControl<Date | undefined>(undefined, [deadlineNonRequire]),
        tasks: new FormArray([], [Validators.nullValidator]),
        type: new FormControl(''),
        attributes: new FormArray([
          
        ],[Validators.nullValidator])
      });

export type ProjectType = typeof schema._output.type;

export const ProjectTypeMapping: Record<ProjectType, string> = {
  "PERSONAL": "Personal",
  "PRO": "Professional"
}

export const parseProject = map(p => schema.parse(p))
export const parseProjectList = map( (projects: unknown[]) => projects.map(x => schema.parse(x)  ))