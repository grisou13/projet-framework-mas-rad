import { NgModule, inject } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectLayoutComponent } from './layout/project-layout.component';
import { authGuard } from '../auth/guards/auth-guard.guard';
import { ListComponent } from './list/list.component';
import { ProjectForm } from './form/form.component';
import { EditComponent } from './edit/edit.component';

export const routes: Routes = [
  {
    path: '',
    component: ProjectLayoutComponent,
    canActivateChild: [authGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        // redirectTo: "/dashboard"
        //canActivate: [anonGuard],
        component: ListComponent,
        title: 'Mes projets',
        data: {
          title: 'Mes projets',
        },
      },
      {
        path: 'new',
        pathMatch: 'full',
        //canActivate: [anonGuard],
        component: ProjectForm,
        title: 'Nouveau projets',
        data: {
          title: 'Nouveau projets',
        },
      },
      {
        path: ':projectId',
        component: EditComponent,
        title: 'Edit my projets',
      },
    ],
  },
];
