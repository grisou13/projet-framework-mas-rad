import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { TasksService } from './services/tasks.service';
import { TaskLayoutComponent } from './layout/task-layout.component';
import { ListComponent } from './list/list.component';
import { RouterModule } from '@angular/router';
import { routes } from './tasks.routes';



@NgModule({
  declarations: [TaskLayoutComponent, ListComponent],
  providers: [
    TasksService
  ],
  imports: [SharedModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TasksModule { 

  static forRoot(): ModuleWithProviders<TasksModule> {
    return {
      ngModule: TasksModule,
      providers: [
        TasksService
      ],
    };
  }
}
