import { Component, Input, inject } from '@angular/core';
import { TasksService } from '../services/tasks.service';
import { Task } from '../schema';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, map, switchMap } from 'rxjs';
import { BadgeColor } from '@shared/components/status-badge/status-badge.component';

@Component({
  templateUrl: './list.component.html',
})
export class ListComponent {
  taskService = inject(TasksService);

  reload = new BehaviorSubject<null>(null);

  loadable$ = this.route.queryParamMap.pipe(switchMap( params => {
    const projectId = params.get("projectId");
    if(projectId != null)
     return this.taskService.getForProject(projectId);

     return this.taskService.getAll();
  }));

  constructor(private route:ActivatedRoute){}

  newTaskLink$ = this.route.queryParamMap.pipe(map(params => {
    if(params.has("projectId")){
      return "/tasks/new?projectId="+params.get("projectId");
    }
    return "/tasks/new";
  }));

  taskBadge(task: Task){
    if(task.done) return BadgeColor.Green;
    const now = new Date();
    if(task.deadline && task.deadline >= now) return BadgeColor.Red;

    return BadgeColor.Dark;
  }

  taskStatus(task: Task){
    if(task.done) return "Done";
    const now = new Date();
    if(task.deadline && task.deadline >= now) return "Late";
    
    return "On going";
  }
  
}
