import { Injectable, inject } from '@angular/core';


import { HttpClient } from '@angular/common/http';
import { Task, UpsertTask, parseTask, parseTaskList, schema, validator } from '../schema';
import { concatAll, from, map, mergeAll, of, switchMap, toArray } from 'rxjs';
import { Project, parseProjectList } from 'src/app/projects/schema';



@Injectable({
  providedIn: 'root',
})
export class TasksService {
  http = inject(HttpClient);
  
  getProjects = () => this.http.get<Project[]>("/projects").pipe(parseProjectList);
  getForProject = (projectId: string) => this.http.get<Task[]>('/tasks?projectId='+projectId).pipe(parseTaskList);
  
  getAll = () => this.http.get<Task[]>('/tasks').pipe(parseTaskList);
  getSingle = (id: string) => this.http.get<Task>('/tasks/'+id).pipe(parseTask);
  
  updateSingle = (id: string, payload: UpsertTask) => from(validator.parseAsync(payload)).pipe(switchMap( p => this.http.put('/tasks/'+id, p)));
  create = (payload: UpsertTask) =>  from(validator.parseAsync(payload)).pipe(switchMap( p => this.http.post('/tasks', p)));
  
}
