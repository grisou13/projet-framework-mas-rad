import { Component, EventEmitter, Input, OnInit, Output, inject } from '@angular/core';
import { Task, TaskTypeMapping, UpsertTask, taskForm } from '../schema';
import { FormArray, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { BehaviorSubject, EMPTY, map, merge, of, switchMap, tap, zip } from 'rxjs';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, ActivatedRouteSnapshot, ParamMap, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { TasksService } from '../services/tasks.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { SharedModule } from '@shared/shared.module';
import { handleAppError } from '@shared/extensions/handleErrorRx.pipe';
import {Location} from '@angular/common';
import { handleSuccess } from '@shared/extensions/handleSuccessRx.pipe';
import { ToastrService } from 'ngx-toastr';
@Component({
  standalone: true,
  selector: "app-task-form",
  imports: [ReactiveFormsModule, CommonModule, SharedModule],
  templateUrl: './form.component.html'
})
export class TaskForm {
  @Input() taskId : string | null = null;

  location = inject(Location);
  router = inject(Router);

  taskType = TaskTypeMapping
  loading$ = new BehaviorSubject(false);
  toastr = inject(ToastrService)
  form = taskForm();

  create$ =this.route.paramMap.pipe(
    switchMap( params => 
      this.taskService.create(this.form.value as UpsertTask)
    ),
    handleAppError((err) => ({
        message: "We could not create task ",
        status: "",
        context: "task",
        error: err
    })),
    handleSuccess((val) => `Task ${(val as Task).id} created`)
  );

  update$ = this.route.paramMap.pipe(
    switchMap( params => 
      this.taskService.updateSingle(params.get('taskId')!, this.form.value as UpsertTask)
    ),
    handleAppError((err) => ({
        message: "We could not update task ",
        status: "",
        context: "task",
        error: err
    })),
    handleSuccess((val) => `Task ${(val as Task).id} update`)
  )

  saveable$ =  this.route.paramMap.pipe(
    switchMap(params => 
      params.has('taskId') ? 
      this.update$ : 
      this.create$
    ),
    
    tap({
      next: (_) => {
        this.router.navigate(["/"], {relativeTo: this.route})
      }
    }),
    takeUntilDestroyed()
  );


  invalid$ = this.form.statusChanges.pipe(map(x => x === 'INVALID'));
  
  constructor(private route: ActivatedRoute, private taskService: TasksService) { }
    
  projectId$ = zip([this.route.queryParamMap, this.route.paramMap]).pipe(map( ([queryMap, paramMap]) => {
    if( queryMap.has("projectId") ) {
      return parseInt(queryMap.get("projectId") ?? "")
    }
    if(paramMap.has("projectId")){
      return parseInt(queryMap.get("projectId") ?? "");
    }

    return null;
  }))


  task$ = this.route.paramMap.pipe(map(params => {
    return this.taskId != null ? this.taskId : params.get('taskId')
  }), switchMap( id => {
    return id === null ? of(null) : this.taskService.getSingle(id)
  }));
  
  relatedTasks$ = this.projectId$.pipe(
    switchMap( (projectId) => {
      if(projectId == null){
        return of([]);
      }
      return this.taskService.getForProject(projectId.toString());
    })
  );
  projects$ = this.taskService.getProjects()
  loadable$ = zip([this.task$, this.projectId$, this.relatedTasks$, this.projects$]).pipe(
    tap({next: ([task, projectId, relatedTasks]) => {
    const tsk: Partial<Task> | null = {
      ...(task || {}),
      projectId: task?.projectId ?? projectId
    };
    this.bootstrapFormValue(tsk);
  }}),
    map( ([task, projectId, relatedTasks, projects]) => {
      return {tasks: relatedTasks.reduce( (acc, cur) => {
          acc[cur.id] = cur.content.trim();
          return acc;
        },{'': 'None'} as {[index: string]: string}),
        projects: projects.reduce( (acc, cur) => {
          acc[cur.id] = cur.title.trim();
          return acc;
        },{'': 'None'} as {[index: string]: string})
      };

    })
  );



  protected bootstrapFormValue(data: Partial<Task> | null)
  {
    if(data === null){
      return;
    }
    this.form.patchValue({
      id: data?.id ?? undefined,
      content: data?.content ?? undefined,
      deadline: data.deadline ?? undefined,
      type: data?.type ?? "TASK",
      responsibleId: data?.responsibleId ?? undefined,
      parentId: data?.parentId ?? undefined,
      projectId: data?.projectId ?? undefined,
      done: data?.done ?? false,
      doneAt: data?.doneAt ?? null,
    });  
  }

  
  submit() {
    if(this.form.invalid){
      return;
    }
    const parentId = this.form.get("parentId")
    if(parentId != null && parentId.value != null && parentId.value.toString().length <= 0){
      parentId.setValue(null);
      parentId.updateValueAndValidity({ onlySelf: true})
    }
    const responsibleId = this.form.get("responsibleId");

    if(responsibleId != null && responsibleId.value != null && responsibleId.value.toString().length <= 0){
      responsibleId.setValue(null);
      responsibleId.updateValueAndValidity({ onlySelf: true})
    }
    
    const projectId = this.form.get("projectId");
    if(projectId  != null && projectId.value != null && projectId.value.toString().length <= 0){
      projectId.setValue(null);
      projectId.updateValueAndValidity({ onlySelf: true})
    }

    console.log(this.form.value);

    this.saveable$.subscribe();
  }

  getControl(name: string){
    return this.form.get(name) as FormControl;
  }

  formStatus() {
    return Object.keys(this.form.controls).map(k => `${k}: ${this.form.get(k)?.status}`).join(' - ')
  }

}
