import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';

@Component({
  templateUrl: './task-layout.component.html',
  styleUrls: [],
})
export class TaskLayoutComponent implements OnInit {
  title: string | undefined = '';
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.title = this.route.firstChild?.snapshot.title || '';
  }
}
