import { NgModule, inject } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TaskLayoutComponent } from './layout/task-layout.component';
import { authGuard } from '../auth/guards/auth-guard.guard';
import { ListComponent } from './list/list.component';
import { TaskForm } from './form/form.component';

export const routes: Routes = [
  {
    path: '',
    component: TaskLayoutComponent,
    canActivateChild: [authGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        //canActivate: [anonGuard],
        component: ListComponent,
        title: 'My tasks',
      },
      {
        path: 'new',
        pathMatch: 'full',
        //canActivate: [anonGuard],
        component: TaskForm,
        title: 'Nouvelle tâche',

      },
      {
        path: ':taskId',
        component: TaskForm,
        title: 'Edit my task',
      },
    ],
  },
];
