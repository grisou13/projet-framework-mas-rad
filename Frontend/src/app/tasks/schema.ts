import { AbstractControl, FormArray, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { map } from 'rxjs';
import { z } from 'zod';

export const validator = z.object({
    content: z.string().min(3).max(250),
    deadline: z.coerce.date().nullable().optional(), //z.string().transform((str) => new Date(str ?? '')).optional().nullable(),
    type: z.union([z.literal('TASK'), z.literal('ISSUE'), z.literal('BACKLOG')]).default('TASK'),
    done: z.boolean().optional().nullable().default(false),
    doneAt: z.coerce.date().optional().nullable(), //z.string().transform(str => new Date(str ?? '')).optional().nullable(),
    parentId: z.number().optional().nullable(),
    projectId: z.number().optional().nullable(),
    responsibleId: z.number().optional().nullable()
});

export const schema = z
.object({
    id: z.number(),
}).merge(validator);


export type UpsertTask = z.infer<typeof validator>;

export type Task = z.infer<typeof schema>;

export function deadlineNonRequire(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control) {
      return null;
    }
    
    if (control.getRawValue()?.length > 0 ?? false) {
      return Validators.compose([ Validators.required, Validators.pattern("[0-9]{4}-[0-9]{2}-[0-9]{2}") ]); 
    }
    return null;
  };
    
}


export function nonEmptyInput(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control) {
      return null;
    }
    
    if (control.value?.length > 0 ?? false) {
      return Validators.compose([ Validators.required ]); 
    }
    return null;
  };
    
}
export const taskForm = () => new FormGroup({
    id: new FormControl<number>(-1, [Validators.nullValidator]),
    content: new FormControl<string>('',[Validators.required, Validators.minLength(1)]),
    deadline: new FormControl<Date | undefined>(undefined, [ deadlineNonRequire ]),
    type: new FormControl<string>('TASK', []),
    done: new FormControl<boolean>(false, []),
    doneAt: new FormControl<Date | undefined>(undefined, [ deadlineNonRequire ]),
    parentId: new FormControl<number | undefined>(undefined, []),
    projectId: new FormControl<number | undefined>(undefined, []),
    responsibleId: new FormControl<number | undefined>(undefined, [])
})

export type TaskType = typeof schema._output.type;

export const TaskTypeMapping: Record<TaskType, string> = {
  "BACKLOG": "Backlog",
  "TASK": "Default",
  "ISSUE": "Issue"
}

export const parseTask = map(p => schema.parse(p))
export const parseTaskList = map( (tasks: unknown[]) => tasks.map(x => schema.parse(x)  ));