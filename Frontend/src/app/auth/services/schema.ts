import { z } from 'zod';

export const userValidator = z.object({
    username: z.string().min(3).max(25), // "Somewhere",
    password: z.string().min(4), //"Over the rainbow",
});
export const schema = z
.object({
    roles: z.array(z.string()),
})
.merge(userValidator.omit({ password: true }));


export type AuthParams = z.infer<typeof userValidator>;

export type User = z.infer<typeof schema>;
