import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  Observable,
  delay,
  of,
  shareReplay,
  startWith,
  switchMap,
  tap,
} from 'rxjs';
import {
  AuthParams,
  User
} from './schema';

import { AUTH_PWD, AUTH_USER, fetchPassword } from '@shared/credentials';
import { HttpClient } from '@angular/common/http';


export type Authentication = {
  authMethod: 'default';
  credential: string;
  password: string;
};
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isAuthenticatedEvent = new BehaviorSubject<boolean>(false);
  public IsAuthenticated$: Observable<boolean> = this.isAuthenticatedEvent
    .asObservable()
    .pipe(shareReplay(1));
  //.pipe(shareReplay({ bufferSize: 1, refCount: false }));

  private userEvent = new BehaviorSubject<null | User>(null);
  public user$: Observable<null | User> = this.IsAuthenticated$.pipe(
    switchMap((authenticated) => {
      if (!authenticated) {
        return of(null);
      }
      return this.resolveUser().pipe(shareReplay(1));
      //return this.userEvent.asObservable();
    }),
    shareReplay(1)
  );
  //.pipe(shareReplay({ bufferSize: 1, refCount: false }));

  constructor(private http: HttpClient) {
    console.debug('Creating auth service');
    this.isAuthenticatedEvent.next(this.isTokenValid());
  }

  public boot() {
    console.debug('Booting auth service');
    return this.resolveUser().pipe(
      tap((user) => {
        if (!user) return;
        this.userEvent.next(user);
      })
    );
  }

  public getToken() {
    return fetchPassword();
  }

  public isTokenValid() {
    return this.getToken() != null && (this.getToken()?.length || 0) > 0;
    // the api actually doesn't give a jwt token but a generic token
    // return !this.jwtHelper.isTokenExpired(this.getToken());
  }

  public logout() {
    console.log("loging out")
    localStorage.removeItem(AUTH_USER);
    localStorage.removeItem(AUTH_PWD);
    this.isAuthenticatedEvent.next(false);
    this.userEvent.next(null);
  }

  public signup(data: AuthParams) {
    return this.http.post("/auth/register",data).pipe(
      switchMap((res) =>
        this.authenticate({
          authMethod: 'default',
          credential: data.username,
          password: data.password,
        })
      )
    );
  }

  public authenticate(
    authInfo: Authentication
  ): Observable<User | null> {
    return this.http
      .post<User>("/auth/login",{
        username: authInfo.credential,
        password: authInfo.password,
      })
      .pipe(
        tap((res) => {
          this.isAuthenticatedEvent.next(true);
          this.userEvent.next(res);
          localStorage.setItem(AUTH_USER, authInfo.credential);
          localStorage.setItem(AUTH_PWD, authInfo.password);
        })
      );
  }

  private resolveUser(): Observable<null | User> {
    //resolve user on startup, this is usefull so we don't keep the whole user object
    if (!this.isTokenValid()) {
      return of(null);
    }
    return this.http.get<User>("/auth/me");
  }
}
