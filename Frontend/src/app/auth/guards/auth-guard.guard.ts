import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AuthService } from '../services/auth.service';

export const authGuard: CanActivateFn = (
  route: ActivatedRouteSnapshot,
) => {
  const router = inject(Router);
  const authService = inject(AuthService);
  const isAuthenticated = authService.isTokenValid();
  if (isAuthenticated) {
    return true;
  }
  router.navigate(['/auth/login'], {
    queryParams: { returnUrl: (route.data["returnUrl"] || "/dashboard") },
  });
  return false;
  /*
  return authService.IsAuthenticated$.pipe(
    switchMap((isAuthenticated) => {
      if (isAuthenticated) {
        return of(true);
      }
      return of(router.navigate(['/login'])).pipe(map((_) => false));
      // return false;
    })
  );*/
};
