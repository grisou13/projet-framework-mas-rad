import { NgModule, inject } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardLayoutComponent } from './layout/dashboard-layout.component';
import { authGuard } from '../auth/guards/auth-guard.guard';
import { IndexComponent } from './index/index.component';

export const routes: Routes = [
  {
    path: '',
    component: DashboardLayoutComponent,
    canActivateChild: [authGuard],
    children: [
      {
        path: '',
        pathMatch: "full",
        //canActivate: [anonGuard],
        component: IndexComponent,
        title: 'Index',
        data: {
          title: 'Index',
        },
      },
      // {
      //   path: 'new-task',
      //   component: NewTaskComponent
      // }
    ],
  },
];
