import { Component, inject } from '@angular/core';
import { DashboardService } from '../services/dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, switchMap } from 'rxjs';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
})
export class IndexComponent {
  route = inject(ActivatedRoute);
  dashboardService = inject(DashboardService);
  navigator = inject(Router);

  project = new BehaviorSubject<string | null>(null);

  projects$ = this.dashboardService.getDashboard();
  



  

  backlog$ = this.dashboardService.getBacklog();
  handleNodeClick($event: { type: string; id: string; }) {
    switch($event.type.toUpperCase()){
      case "PROJECT":
        this.navigator.navigate(["./projects/"+$event.id], {relativeTo: this.route});
        break;
      case "TASK":
        this.navigator.navigate(["./tasks/"+$event.id], {relativeTo: this.route})
        break;
      default:
        break;
    }
  }
}
