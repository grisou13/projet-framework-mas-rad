import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router, RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { DashboardService } from '../services/dashboard.service';
import { BehaviorSubject, combineLatest, filter, switchMap, zip } from 'rxjs';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: [],
})
export class DashboardLayoutComponent implements OnInit {
  route = inject(ActivatedRoute);
  dashboardService = inject(DashboardService);
  navigator = inject(Router);

  project = new BehaviorSubject<string | null>(null);

  projects$ = this.dashboardService.getDashboard();
  
  handleNodeClick($event: { type: string; id: string; }) {
    switch($event.type.toUpperCase()){
      case "PROJECT":
        this.project.next($event.id);
        this.navigator.navigate(["/projects/"+$event.id], {relativeTo: this.route});
        break;
      case "TASK":
        this.navigator.navigate(["/tasks/"+$event.id], {relativeTo: this.route})
        break;
      default:
        break;
    }
  }
  title: string | undefined = '';

  ngOnInit() {
    this.title = this.route.firstChild?.snapshot.title || '';
  }
}
