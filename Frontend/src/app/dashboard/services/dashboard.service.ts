import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { map } from 'rxjs';
import { Project, parseProjectList } from 'src/app/projects/schema';
import { Task, parseTaskList } from 'src/app/tasks/schema';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  getDashboardForProject = (projectId: string) => this.http.get<Project>("/projects/"+projectId).pipe(map(values => {
    return [values];
  }))

  getDashboard = () => this.http.get<Project[]>("/projects").pipe(parseProjectList);
  getBacklog = () => this.http.get<Task[]>("/tasks").pipe(
    map(tasks => tasks.filter(t => t.projectId === null)),
    parseTaskList
  );
  http = inject(HttpClient);
}
