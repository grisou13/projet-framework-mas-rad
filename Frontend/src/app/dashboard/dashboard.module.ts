import { APP_INITIALIZER, ModuleWithProviders, NgModule } from '@angular/core';

import { routes } from './dashboard-routing.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { DashboardLayoutComponent } from './layout/dashboard-layout.component';
import { IndexComponent } from './index/index.component';
import { GraphVizComponent } from '@shared/components/graph-viz/graph-viz.component';
import { DashboardService } from './services/dashboard.service';
import { BacklogComponent } from './backlog/backlog.component';
import { TaskForm } from '../tasks/form/form.component';
import { NewTaskComponent } from '../tasks/components/new-task/new-task.component';


@NgModule({
  declarations: [DashboardLayoutComponent, IndexComponent, BacklogComponent],
  providers: [
    DashboardService
  ],
  imports: [SharedModule, GraphVizComponent, TaskForm, NewTaskComponent, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardModule { 
  static forRoot(): ModuleWithProviders<DashboardModule> {
    return {
      ngModule: DashboardModule,
      providers: [
        DashboardModule
      ],
    };
  }
}
