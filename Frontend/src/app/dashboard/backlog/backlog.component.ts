import { Component, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from '../services/dashboard.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-backlog',
  templateUrl: './backlog.component.html',
})
export class BacklogComponent {

  route = inject(ActivatedRoute);
  dashboardService = inject(DashboardService);
  navigator = inject(Router);

  backlog$ = this.dashboardService.getBacklog();

}
