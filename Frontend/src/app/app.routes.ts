import { Routes } from '@angular/router';
import { anonGuard } from './auth/guards/anon-guard.guard';
import { IndexComponent } from './pages/index/index.component';

export const routes: Routes = [
    {
        path: '',
        canActivate: [anonGuard],
        pathMatch: 'full',
        component: IndexComponent,
    },
    {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    },
    {
        path: 'projects',
        loadChildren: () => import('./projects/projects.module').then(m => m.ProjectsModule),
      },
      {
          path: 'tasks',
          loadChildren: () => import('./tasks/tasks.module').then(m => m.TasksModule),
      },

    
    {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
    },
];
