import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import Graph from "graphology";
import Sigma from "sigma";
import { Project } from 'src/app/projects/schema';

@Component({
  selector: 'app-graph-viz',
  standalone: true,
  imports: [],
  templateUrl: './graph-viz.component.html',
})
export class GraphVizComponent implements AfterViewInit {
  renderer!: Sigma<any>;

  @Input() projects: Project[] = [];

  @Output() onClick = new EventEmitter<{type: string /*"TASK" | "PROJECT"*/, id: string}>;

  @ViewChild('graphContainer') container!: ElementRef<HTMLDivElement>;

  buildGraph(){
    const graph = new Graph();

    this.projects.forEach( (p,i )  => {
      const angle = (360 * i) / (this.projects.length);
      const basePos = i * (5 - Math.max(2 * Math.random(), 2));
      const basePosX = basePos * Math.cos(angle);
      const basePosY = basePos * Math.sin(angle);

      graph.addNode("project-"+p.id, { x: basePosX, y: basePosY, size: 10, label: p.title + "("+p.tasks.length+")", color: "blue"});
      

      p.tasks.forEach( (t, i) => {
        const angle = (360 * i) / (p.tasks.length);
        const baseLen = 4 - Math.max(1 * Math.random(), 0);
        graph.addNode("task-"+t.id, { x: basePosX + baseLen * Math.cos(angle), y: basePosY + baseLen * Math.sin(angle), size: 5, label: t.content, color: "red" })
        if(t.parentId != null){
          graph.addEdge("task-"+t.id, "task-"+t.parentId);
        }
        else{
          graph.addEdge("project-"+p.id, "task-"+t.id)
        }
      })
    })
    return graph;
  }
  renderGraph(graph: any)
  {

    this.renderer = new Sigma(graph, this.container.nativeElement);

    this.renderer.on('clickNode', (payload) => {
      const nodeId = payload.node;
      const [type, id] = nodeId.split("-");
      this.onClick.emit({type: type.toUpperCase(), id})
    })
  }
  displayGraph(){
    if(this.container == null){
      return;
    }
    if(this.renderer != null){
      this.renderer.clear();
    }
    this.renderGraph(this.buildGraph());
    
  }
  ngAfterViewInit(): void {
    console.log("After view init");
    this.displayGraph();
  }

}
