import { CommonModule } from '@angular/common';
import { Component, Input, forwardRef } from '@angular/core';
import { FormControl, FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-select-input',
  templateUrl: './select-input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectInputComponent),
      multi: true,
    },
  ],
})
export class SelectInputComponent {
  @Input() label: string = "";
  @Input() name: string = "";
  @Input({required: true}) values! : Record<any, string>

  public get types(){
    return Object.keys(this.values);
  }
  @Input() control: FormControl = new FormControl('');
}
