import { Component, OnInit, inject } from '@angular/core';
import { Router } from '@angular/router';

import { distinctUntilChanged, shareReplay } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [],
})
export class NavbarComponent implements OnInit {
  router = inject(Router)
  constructor(private authService: AuthService) {}

  isAuthenticated$ = this.authService.IsAuthenticated$.pipe(
    distinctUntilChanged(),
    shareReplay(1)
  );

  ngOnInit(): void {
  }

  logout() {
    this.authService.logout();
    this.router.navigate(["/"]);
  }
}
