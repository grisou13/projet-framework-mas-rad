import { ChangeDetectionStrategy, Component, CUSTOM_ELEMENTS_SCHEMA, DestroyRef, EventEmitter, forwardRef, inject, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, FormControl, FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule, Validators } from '@angular/forms';
import { debounceTime, noop, tap } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { CommonModule } from '@angular/common';
import { injectNgControl } from './input';
import { NoopValueAccessorDirective } from '@shared/directives/noop-value-accessor.directive';

@Component({
  selector: 'app-text-input',
  templateUrl: 'input.component.html',
  styleUrls: [],
  // changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true,
    },
  ],
})
export class InputComponent /* implements ControlValueAccessor/*, OnInit */ {

  @Input() label: string = "";
  @Input() name: string = "";
  @Input() autofocus = false;
  @Input() type = "text";
  @Input() required = true;
  
  @Input() control: FormControl = new FormControl('');

}
  /*ngControl = injectNgControl();
  // public formControl: FormControl = new FormControl<string>('');

  // destroyRef: DestroyRef = inject(DestroyRef);

  // onChange: (value: string) => void = noop;
  // onTouch: () => void = noop;

  // registerOnChange(fn: (value: string) => void): void {
  //   this.onChange = fn;
  // }

  // registerOnTouched(fn: () => void): void {
  //   this.onTouch = fn;
  // }

  // setDisabledState(isDisabled: boolean): void {
  //   isDisabled ? this.formControl.disable() : this.formControl.enable();
  // }

  // writeValue(value: string): void {
  //   this.formControl.setValue(value, { emitEvent: false });
  // }

  // ngOnInit(): void {
  //   this.formControl.valueChanges
  //     .pipe(
  //       debounceTime(200),
  //       tap(value => this.onChange(value)),
  //       takeUntilDestroyed(this.destroyRef),
  //     )
  //     .subscribe();
  // }
}
*/