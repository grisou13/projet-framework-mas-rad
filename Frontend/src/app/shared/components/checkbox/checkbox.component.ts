import { Component, Input, forwardRef } from '@angular/core';
import { FormControl, FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-checkbox',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true,
    },
  ],
  templateUrl: './checkbox.component.html',
  styles: ``
})
export class CheckboxComponent {
  @Input() label: string = "";
  @Input() name: string = "";
  @Input() autofocus = false;
  @Input() type = "text";
  @Input() required = true;
  
  @Input() control: FormControl = new FormControl('');
}
