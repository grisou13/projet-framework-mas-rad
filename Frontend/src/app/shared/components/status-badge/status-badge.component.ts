import { Component, Input } from '@angular/core';

export enum BadgeColor {
  Default,
  Dark,
  Red,
  Green,
  Yellow,
  Indigo,
  Purple,
  Pink
}

@Component({
  selector: 'app-status-badge',
  standalone: true,
  imports: [],
  templateUrl: './status-badge.component.html'
})
export class StatusBadgeComponent {
  readonly C = BadgeColor;

  @Input({required: true}) color = BadgeColor.Default;
  @Input() text = "";
}
