import { Component, Input, forwardRef } from '@angular/core';
import { FormControl, FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { DateValueAccessor } from '@shared/directives/use-value-as-date';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputDateComponent),
      multi: true,
    },
  ],
})
export class InputDateComponent {
  @Input() label: string = "";
  @Input() name: string = "";
  @Input() autofocus = false;
  @Input() required = false;
  
  @Input() control = new FormControl<Date | undefined>(undefined);
}
