import { APP_INITIALIZER, ErrorHandler, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WithLoadingPipe } from './pipes/with-loading.pipe';
import { LogoComponent } from './components/logo/logo.component';
import { HighlightDirective } from './directives/highlight.directive';
import { WithLoadingStatusPipe } from './pipes/with-loading-status.pipe';
import { HighlightPipe } from './pipes/highlight.pipe';
import { EditableParagrapheComponent } from './components/editable-paragraphe/editable-paragraphe.component';
import { ContenteditableValueAccessor } from './directives/content-editable.directive';
import { EmptyRouterOutletComponent } from './components/empty-router-outlet/empty-router-outlet.component';
import { RouterModule } from '@angular/router';
import { APP_INTERCEPTORS, BASE_API_URL, BaseUrlInterceptor, BearerAuthTokenInterceptor, CREDENTIAL_LOADER_FACTORY, baseUrl } from './http/interceptors';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { fetchPassword, fetchUsername } from './credentials';
import {Buffer} from 'buffer';
import { NavbarComponent } from './components/navbar/navbar.component';
import { InputComponent } from './components/input/input.component';
import { SelectInputComponent } from './components/select-input/select-input.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { AppErrorHandler } from './app-error-handler';
import { ErrorHandlerService } from './error-handler.service';
import { ErrorHandlingInterceptor } from './error-handling.interceptor';
import { InputDateComponent } from './components/input-date/input-date.component';
import { DateValueAccessor } from './directives/use-value-as-date';
import { SuccessHandlerService } from './success-handler.service';
import { StatusBadgeComponent } from './components/status-badge/status-badge.component';

@NgModule({
  declarations: [
    WithLoadingPipe,
    LogoComponent,
    NavbarComponent,
    HighlightDirective,
    WithLoadingStatusPipe,
    HighlightPipe,
    EditableParagrapheComponent,
    ContenteditableValueAccessor,
    EmptyRouterOutletComponent,
    InputComponent,
    SelectInputComponent,
    CheckboxComponent,
    InputDateComponent,
    DateValueAccessor
  ],
  providers: [
    {
      provide: BASE_API_URL,
      useFactory: baseUrl,
      deps: [],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BearerAuthTokenInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BaseUrlInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlingInterceptor,
      multi: true
    },
    ErrorHandlerService,
    SuccessHandlerService,
    {provide: ErrorHandler, useClass: AppErrorHandler},
    {
      provide: CREDENTIAL_LOADER_FACTORY,
      useValue: () => {
        const username = fetchUsername();
        const pwd = fetchPassword();
        if(username == null || pwd == null){
            return null;
        }
        const credentials = username + ":" + pwd;
        return `Basic ${Buffer.from(credentials).toString("base64") }`;
      }
    },

    HttpClient
  ],
  imports: [CommonModule, ReactiveFormsModule, StatusBadgeComponent, FormsModule, RouterModule, HttpClientModule],
  exports: [
    ContenteditableValueAccessor,
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    LogoComponent,
    WithLoadingPipe,
    HighlightDirective,
    WithLoadingStatusPipe,
    NavbarComponent,
    EditableParagrapheComponent,
    InputComponent,
    SelectInputComponent,
    CheckboxComponent,
    InputDateComponent,
    DateValueAccessor,
    StatusBadgeComponent
  ],
})
export class SharedModule {
  
}
