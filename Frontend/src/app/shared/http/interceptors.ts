import {
    HttpBackend,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
  } from '@angular/common/http';
  import { Inject, Injectable, InjectionToken } from '@angular/core';
  import { Observable } from 'rxjs';
  
  import { environment } from '@env';

  export const baseUrl = () => environment.taskApi;

  
  export const BASE_API_URL = new InjectionToken<string>(
    'Auth api base url'
  );

  export const APP_INTERCEPTORS = new InjectionToken<
    HttpInterceptor[]
  >('An abstraction on feature HttpInterceptor[]');
  
  export const CREDENTIAL_LOADER_FACTORY = new InjectionToken<
    HttpInterceptor[]
  >('CREDENTIAL_LOADER_FACTORY that loads credentials from storage for http authorization features');
  
  export class InterceptorHandler implements HttpHandler {
    constructor(
      private next: HttpHandler,
      private interceptor: HttpInterceptor
    ) {}
  
    handle(req: HttpRequest<any>): Observable<HttpEvent<any>> {
      return this.interceptor.intercept(req, this.next);
    }
  }
  export class InterceptingHandler implements HttpHandler {
    private chain: HttpHandler;
  
    constructor(
      private backend: HttpBackend,
      private interceptors: HttpInterceptor[]
    ) {
      this.chain = this.interceptors.reduceRight(
        (next, interceptor) => new InterceptorHandler(next, interceptor),
        this.backend
      );
    }
  
    handle(req: HttpRequest<any>): Observable<HttpEvent<any>> {
      return this.chain.handle(req);
    }
  }
  

@Injectable()
export class BearerAuthTokenInterceptor implements HttpInterceptor {
    constructor(@Inject(CREDENTIAL_LOADER_FACTORY)private credentialLoader : () => string | null){

    }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const credentials = this.credentialLoader();

    
    if(credentials == null){
      return next.handle(request);
    }


    return next.handle(
      request.clone({
        setHeaders: {
          Authorization: credentials,
        },
      })
    );
  }
}

@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {
  constructor(@Inject(BASE_API_URL) private baseUrl: string) {
    this.baseUrl =
      this.baseUrl.charAt(this.baseUrl.length - 1) == '/'
        ? this.baseUrl.substr(0, this.baseUrl.length - 1) //remove trailing /
        : this.baseUrl;
  }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const url =
      request.url.charAt(0) === '/' ? request.url.substring(1) : request.url;
    return next.handle(
      request.clone({
        url: `${this.baseUrl}/${url}`,
      })
    );
  }
}