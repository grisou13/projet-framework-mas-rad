export const AUTH_USER = 'auth-username';
export const AUTH_PWD = 'auth-pwd';
export const fetchUsername = () => localStorage.getItem(AUTH_USER);
export const fetchPassword = () => localStorage.getItem(AUTH_PWD);