import { z } from 'zod';


export const validator = z.object({
    key: z.string(),
    value: z.string()
});

export type CustomAttribute = z.infer<typeof validator>;