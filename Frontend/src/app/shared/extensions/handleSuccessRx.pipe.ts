import { Injector, inject } from '@angular/core';
import { SuccessHandlerService } from '@shared/success-handler.service';

import { ToastrService } from 'ngx-toastr';
import { Observable, catchError, finalize, tap } from 'rxjs';

export function handleSuccess<T>(
  messageToToast: <T>(val: T) => string
): (source: Observable<T>) => Observable<T> {
  const srv = inject(SuccessHandlerService);
  return function (source: Observable<T>) {
    return source.pipe(
      tap({
        next: (val) => {
          srv.handleSuccess(messageToToast(val))
        }
      }),
      
    );
  };
}
