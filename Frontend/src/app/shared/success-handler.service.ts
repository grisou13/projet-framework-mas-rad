import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

export type ErrorMessage = {
  message: string;
  status: any;
  context: string;
  error: Error;
};
@Injectable({
  providedIn: 'any'
})
export class SuccessHandlerService {
  constructor(private toastr: ToastrService) {}
  handleSuccess(msg: string): void {
    this.toastr.success(msg);
    //throw new Error('Method not implemented.');
  }
}
